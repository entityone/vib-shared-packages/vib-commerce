This module integrates Drupal Commerce with VIB Pay.

# Installation

* Add the following GIT repository to your composer.json repositories:

```json
{
    "type": "git",
    "url": "https://gitlab.com/entityone/vib-shared-packages/vib-commerce.git"
}
```

* Download module and dependent packages by running `composer require drupal/vib_commerce:version`
  For example `composer require drupal/vib_commerce:^1.0-alpha1`

  Available versions: https://gitlab.com/entityone/vib-shared-packages/vib-commerce/-/tags

* Install module by running `drush en vib_commerce`

# Configuration

* Navigate to `admin/commerce/config/payment-gateways` and add and configure a VIB Pay gateway.
* On your checkout flow (`admin/commerce/config/checkout-flows/manage`), make sure users cannot "Checkout as guest".
  This can be disabled, or you just can disable the whole "Login or continue as guest" pane.
* Add the "VIB Payment process" pane to the Payment step and disable the default "Payment process" pane.
* Next configure the rest of your panes. Keep in mind that all billing info is gathered in the VIB Pay service.
* Navigate to your commerce store(s) (`admin/commerce/config/store-types`)
and configure which VIB projects should be available for your store.
* OPTIONAL: Navigate to your order type(s) (`admin/commerce/config/order-types`)
and configure the completion transition which has to be applied during payment notification.

__NOTE__: Users have to be logged in to complete their purchase.
If this is not the case when proceeding to the "Pay and complete step", the user will be redirected to the VIB Service authorization form.

__NOTE 2__: If you get the error "Payment information and Payment process panes need to be on separate steps." while configuring your panes,
you have to apply following patch: `https://www.drupal.org/files/issues/2019-05-03/commerce_payment-3050770-7.patch`


## VAT integration

If you want to integrate the VIB service tax rates with the Commerce tax module
you can do this by navigating to `admin/commerce/config/tax-types` and adding a tax type of type `VIB remote VAT`.

At this point every product variation can be provided with a VIB Product VAT code that will be used to calculate the VAT.
This field is automatically added to all variations (see `vib_commerce_commerce_product_variation_type_insert()`).

## Job allocation integration

If you want to integrate the "Job allocation codes":

* Configure the VIB Projects that apply to your store(s) on `admin/commerce/config/stores/[STORE-ID]`
* Add a field of type `VIB service API reference` (and target type "Job allocation") to your product(s).
* Configure this field as "Job allocation field" on the configuration form of your product type (`admin/commerce/config/product-types`).

The value of this field will now be used as job allocation on the VIB Service order items.


# Configuration VIB Service

Login as an admin in VIB Service and go to "Settings" -> "Apps" -> "Your app" and dit and configure the URLs

* The notification webhook has to be configured as `https://yourwebsite.com/payment/notify/vib_pay`
* The success callback has to be configured as `https://yourwebsite.com/commerce-vib/checkout/return`
* The failure callback has to be configured as `https://yourwebsite.com/commerce-vib/checkout/cancel`

# Custom development

## Event "PROFILE_SYNC_PRESAVE"

The "PROFILE_SYNC_PRESAVE" is dispatched just after fetching the billing information from VIB Service
and right before storing it in the database.
This allows developers to alter the billing information before it is saved.

A `Drupal\vib_commerce\Event\ProfileSyncEvent` event will be available in the event subscriber.

## Event "VIB_ORDER_FETCHED"

The "VIB_ORDER_FETCHED" is dispatched right after the VIB order was fetched successfully from the API.
This event is only dispatched while fetching the order in the context of the payment provider.

A `Drupal\vib_commerce\Event\VibOrderResponseEvent` event will be available in the event subscriber.

## Event "VIB_ORDER_PRE_SEND"

The "VIB_ORDER_PRE_SEND" is dispatched right before sending a POST request to VIB Service to create a new order.
This allows developers to still alter the order data being sent to the VIB Service.

A `Drupal\vib_commerce\Event\VibOrderRequestEvent` event will be available in the event subscriber.

More info on subscribing to events: <https://www.drupal.org/docs/8/creating-custom-modules/subscribe-to-and-dispatch-events>

## Event "VIB_ORDER_STATUS_UPDATED"

The "VIB_ORDER_STATUS_UPDATED" is dispatched when VIB Services notifies the application an order status was updated.
Order status updates are "Canceled", "Paused", "Refunded", ...

A `Drupal\vib_commerce\Event\VibOrderStatusUpdatedEvent` event will be available in the event subscriber.

More info on subscribing to events: <https://www.drupal.org/docs/8/creating-custom-modules/subscribe-to-and-dispatch-events>
