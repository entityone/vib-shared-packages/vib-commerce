<?php

namespace Drupal\vib_commerce\Client;

/**
 * Class VibOrderErrorCodes.
 *
 * @package Drupal\vib_commerce\Client
 */
final class VibOrderErrorCodes {

  /**
   * Returns a message for a given error code.
   *
   * @param string $error_code
   *   The error code.
   *
   * @return string
   *   The message.
   */
  public static function getMessage($error_code) {
    $map = [
      'E1001' => 'Duplicate order',
      'E1002' => 'Tax not allowed for VIB user',
      'E1003' => 'Invalid data',
      'E1004' => 'Invalid user',
      'E1005' => 'Amount is required',
      'E1006' => 'ShopOrderId is required',
      'E1007' => 'ClientId is required',
      'E1008' => 'Invalid client',
      'E1009' => 'Invalid token',
      'E1010' => 'App not found',
      'E1011' => 'User account not found',
      'E1012' => 'UserId is required',
      'E1013' => 'Item code is required',
      'E1014' => 'Item name is required',
      'E1015' => 'Item description is required',
      'E1016' => 'ItemProductCode is required',
      'E1017' => 'ItemProductCode is not found or inactive',
      'E1018' => 'ItemPrice is required',
      'E1019' => 'ItemQty is required',
      'E1020' => 'Sql error',
      'E1021' => 'Data error',
      'E1022' => 'Payment failed',
      'E1023' => 'Sorry, This coupon is not valid for this user account.',
      'E1024' => 'Job allocation code is required',
      'E1025' => 'This job allocation code does not exist',
      'E1026' => 'Project code is required',
      'E1027' => 'This project code does not exist',
      'E1028' => 'App project code does not exist',
      'E1029' => 'App job allocation code does not exist',
      'E1030' => 'No order items present',
      'E1031' => 'A non-empty request body is required.',
    ];

    return isset($map[$error_code]) ? $error_code . ': ' . $map[$error_code] : $error_code . ': Unknown error';
  }

}
