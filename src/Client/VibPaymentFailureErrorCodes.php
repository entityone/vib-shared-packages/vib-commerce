<?php

namespace Drupal\vib_commerce\Client;

/**
 * Class VibPaymentFailureErrorCodes.
 *
 * @package Drupal\vib_commerce\Client
 */
final class VibPaymentFailureErrorCodes {

  const MANUAL_CANCEL = NULL;
  const MULTI_SAFE_PAY_FAILURE = [1000, 9999];

  /**
   * Returns a message for the corresponding error code.
   *
   * @param string $error_code
   *   The error code.
   *
   * @return string
   *   The message.
   */
  public static function getMessage($error_code) {
    if (empty($error_code)) {
      return 'Manually canceled checkout process';
    }

    if ($error_code >= self::MULTI_SAFE_PAY_FAILURE[0] && $error_code <= self::MULTI_SAFE_PAY_FAILURE[1]) {
      return 'Payment failure in MultiSafePay';
    }

    return 'Unknown error';
  }

}
