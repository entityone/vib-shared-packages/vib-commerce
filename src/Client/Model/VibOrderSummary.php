<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\Core\Url;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderSummary.
 *
 * @package Drupal\vib_commerce\Client\Model
 *
 * This is the object that is returned when a new order is created through POST.
 * This only contains some essential data about the order... hence OrderSummary.
 */
class VibOrderSummary implements VibObjectInterface {

  protected $orderId;
  protected $shopOrderId;
  protected $redirectUrl;

  /**
   * VibOrderSummary constructor.
   *
   * @param $order_id
   * @param $shop_order_id
   * @param $redirect_url
   */
  public function __construct($order_id, $shop_order_id, $redirect_url) {
    $this->orderId = $order_id;
    $this->shopOrderId = $shop_order_id;
    $this->redirectUrl = $redirect_url;
  }

  /**
   * @return string
   */
  public function getOrderId() {
    return $this->orderId;
  }

  /**
   * @return string
   */
  public function getShopOrderId() {
    return $this->shopOrderId;
  }

  /**
   * @return \Drupal\Core\Url
   */
  public function getRedirectUrl() {
    return Url::fromUri($this->redirectUrl);
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['Data']['OrderId'],
      $json['Data']['ShopOrderId'],
      $json['Data']['RedirectURL']
    );
  }

}
