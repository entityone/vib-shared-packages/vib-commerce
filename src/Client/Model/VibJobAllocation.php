<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\vib_service\Client\Model\VibFieldableObjectInterface;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibJobAllocation.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibJobAllocation implements VibObjectInterface, VibFieldableObjectInterface {

  protected $code;
  protected $name;
  protected $projectCode;
  protected $projectName;
  protected $centerCode;
  protected $centerName;

  /**
   * VibJobAllocation constructor.
   *
   * @param $code
   * @param $name
   * @param $project_code
   * @param $project_name
   * @param $center_code
   * @param $center_name
   */
  public function __construct($code, $name, $project_code, $project_name, $center_code, $center_name) {
    $this->code = $code;
    $this->name = $name;
    $this->projectCode = $project_code;
    $this->projectName = $project_name;
    $this->centerCode = $center_code;
    $this->centerName = $center_name;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getProjectCode() {
    return $this->projectCode;
  }

  /**
   * @return string
   */
  public function getProjectName() {
    return $this->projectName;
  }

  /**
   * @return string
   */
  public function getCenterCode() {
    return $this->centerCode;
  }

  /**
   * @return string
   */
  public function getCenterName() {
    return $this->centerName;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLabel() {
    return $this->getName() . ' (' . $this->getCode() . ')';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldKey() {
    return $this->getCode();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#markup' => $this->getName(),
      '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'JobAllocationCode' => $this->getCode(),
      'JobAllocationName' => $this->getName(),
      'ProjectCode' => $this->getProjectCode(),
      'ProjectName' => $this->getProjectName(),
      'CenterCode' => $this->getCenterCode(),
      'CenterName' => $this->getCenterName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['JobAllocationCode'],
      $json['JobAllocationName'],
      $json['ProjectCode'],
      $json['ProjectName'],
      $json['CenterCode'],
      $json['CenterName']
    );
  }

}
