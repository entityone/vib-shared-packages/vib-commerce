<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_price\Price;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibTax.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibTax implements VibObjectInterface {

  protected $name;
  protected $percent;
  protected $amount;

  /**
   * VibTax constructor.
   *
   * @param $name
   * @param $percent
   * @param \Drupal\commerce_price\Price $amount
   */
  public function __construct($name, $percent, Price $amount) {
    $this->name = $name;
    $this->percent = $percent;
    $this->amount = $amount;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return int
   */
  public function getPercent() {
    return $this->percent;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'TaxName' => $this->getName(),
      'TaxPercent' => $this->getPercent(),
      'TaxAmount' => $this->getAmount()->toArray(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['TaxName'],
      $json['TaxPercent'],
      new Price($json['TaxAmount'], 'EUR')
    );
  }

}
