<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_price\Price;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderRefund.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibOrderRefund implements VibObjectInterface {

  protected $id;
  protected $comment;
  protected $adminFee;
  protected $amount;
  protected $subTotal;
  protected $taxTotal;
  protected $total;
  protected $items;
  protected $taxes;

  /**
   * VibOrderRefund constructor.
   *
   * @param $id
   * @param $comment
   * @param \Drupal\commerce_price\Price $adminFee
   * @param \Drupal\commerce_price\Price $amount
   * @param \Drupal\commerce_price\Price $subTotal
   * @param \Drupal\commerce_price\Price $taxTotal
   * @param \Drupal\commerce_price\Price $total
   * @param array $items
   * @param array $taxes
   */
  public function __construct($id, $comment, Price $adminFee, Price $amount, Price $subTotal, Price $taxTotal, Price $total, array $items, array $taxes) {
    $this->id = $id;
    $this->comment = $comment;
    $this->adminFee = $adminFee;
    $this->amount = $amount;
    $this->subTotal = $subTotal;
    $this->taxTotal = $taxTotal;
    $this->total = $total;
    $this->items = $items;
    $this->taxes = $taxes;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getAdminFee() {
    return $this->adminFee;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getSubTotal() {
    return $this->subTotal;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getTaxTotal() {
    return $this->taxTotal;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getTotal() {
    return $this->total;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderRefundItem[]
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibTax[]
   */
  public function getTaxes() {
    return $this->taxes;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Id' => $this->getId(),
      'Comment' => $this->getComment(),
      'AdminFee' => $this->getAdminFee()->toArray(),
      'Amount' => $this->getAmount()->toArray(),
      'SubTotal' => $this->getSubTotal()->toArray(),
      'TaxTotal' => $this->getTaxTotal()->toArray(),
      'Total' => $this->getTotal()->toArray(),
      'Items' => array_map(function (VibOrderRefundItem $item) {
        return $item->toJson();
      }, $this->getItems()),
      'Taxes' => array_map(function (VibTax $tax) {
        return $tax->toJson();
      }, $this->getTaxes()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $admin_fee = isset($json['AdminFee']['Total']) ? new Price($json['AdminFee']['Total'], 'EUR') : new Price('0', 'EUR');

    return new static(
      $json['Id'],
      $json['Comment'],
      $admin_fee,
      new Price($json['Amount']['RefundAmount'], 'EUR'),
      new Price($json['Amount']['RefundSubTotal'], 'EUR'),
      new Price($json['Amount']['RefundTax'], 'EUR'),
      new Price($json['Amount']['RefundTotal'], 'EUR'),
      array_map(function (array $item) {
        return VibOrderRefundItem::createFromJson($item);
      }, $json['Items']),
      array_map(function (array $tax) {
        return VibTax::createFromJson($tax);
      }, $json['Taxes']));
  }

}
