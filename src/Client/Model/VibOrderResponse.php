<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderResponse.
 *
 * @package Drupal\vib_commerce\Client\Model
 *
 * This is the object returned when fetching an existing order.
 */
class VibOrderResponse implements VibObjectInterface {

  /**
   * VibOrderResponse constructor.
   *
   * @param $id
   * @param $orderStatus
   * @param $paymentStatus
   * @param $shopOrderId
   * @param \Drupal\vib_commerce\Client\Model\VibCustomer $customer
   * @param \Drupal\vib_commerce\Client\Model\VibOrderPayment $payment
   * @param \Drupal\vib_commerce\Client\Model\VibBilling $billing
   * @param \Drupal\vib_commerce\Client\Model\VibShipping $shipping
   * @param $invoiceRequired
   * @param array $orderItems
   * @param array $taxes
   * @param \Drupal\vib_commerce\Client\Model\VibOrderAmount $amount
   * @param \DateTimeImmutable $createdDate
   * @param \DateTimeImmutable $modifiedDate
   * @param \DateTimeImmutable $paymentDate
   */
  public function __construct(
    protected $id,
    protected $orderStatus,
    protected $paymentStatus,
    protected $shopOrderId,
    protected VibCustomer $customer,
    protected VibOrderPayment $payment,
    protected VibBilling $billing,
    protected VibShipping $shipping,
    protected $invoiceRequired,
    protected array $orderItems,
    protected array $taxes,
    protected VibOrderAmount $amount,
    protected \DateTimeImmutable $createdDate,
    protected \DateTimeImmutable $modifiedDate,
    protected \DateTimeImmutable $paymentDate
  ) {}

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   * @deprecated use getPaymentStatus().
   */
  public function getStatus() {
    return $this->getPaymentStatus();
  }

  /**
   * @return string
   */
  public function getOrderStatus() {
    return $this->orderStatus;
  }

  /**
   * @return string
   */
  public function getPaymentStatus() {
    return $this->paymentStatus;
  }

  /**
   * @return bool
   */
  public function isCompleted() {
    return $this->orderStatus == 'completed';
  }

  /**
   * @return bool
   */
  public function paymentIsCompleted() {
    return $this->paymentStatus == 'completed';
  }

  /**
   * @return bool
   */
  public function isCanceled() {
    return $this->orderStatus == 'cancelled';
  }

  /**
   * @return bool
   */
  public function isPartiallyCanceled() {
    return $this->orderStatus == 'partial_cancelled';
  }

  /**
   * @return bool
   */
  public function isPaused() {
    return $this->orderStatus == 'paused';
  }

  /**
   * @return bool
   */
  public function isRefunded() {
    return $this->orderStatus == 'refund';
  }

  /**
   * @return bool
   */
  public function paymentIsRefunded() {
    return $this->paymentStatus == 'refunded';
  }

  /**
   * @return string
   */
  public function getShopOrderId() {
    return $this->shopOrderId;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibCustomer
   */
  public function getCustomer() {
    return $this->customer;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderPayment
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibBilling
   */
  public function getBilling() {
    return $this->billing;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibShipping
   */
  public function getShipping() {
    return $this->shipping;
  }

  /**
   * @return bool
   */
  public function getInvoiceRequired() {
    return $this->invoiceRequired;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderItemResponse[]
   */
  public function getOrderItems() {
    return $this->orderItems;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibTax[]
   */
  public function getTaxes() {
    return $this->taxes;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderAmount
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * @return \DateTimeImmutable
   */
  public function getCreatedDate() {
    return $this->createdDate;
  }

  /**
   * @return \DateTimeImmutable
   */
  public function getModifiedDate() {
    return $this->modifiedDate;
  }

  /**
   * @return \DateTimeImmutable
   */
  public function getPaymentDate() {
    return $this->paymentDate;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $order_items = [];
    foreach ($json['Items'] as $item) {
      $order_items[] = VibOrderItemResponse::createFromJson($item);
    }

    $taxes = [];
    foreach ($json['Taxes'] as $tax) {
      $taxes[] = VibTax::createFromJson($tax);
    }

    return new static(
      $json['Id'],
      $json['OrderStatus'],
      $json['PaymentStatus'],
      $json['ShopOrderID'],
      VibCustomer::createFromJson($json['Customer']),
      VibOrderPayment::createFromJson($json['Payment']),
      VibBilling::createFromJson($json['Billing']),
      VibShipping::createFromJson($json['Shipping']),
      $json['InvoiceRequired'],
      $order_items,
      $taxes,
      VibOrderAmount::createFromJson($json['Amount']),
      new \DateTimeImmutable($json['CreatedDate'] ?? 'now'),
      new \DateTimeImmutable($json['ModifiedDate'] ?? 'now'),
      new \DateTimeImmutable($json['PaymentDate'] ?? 'now')
    );
  }

}
