<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\vib_service\Client\Model\VibFieldableObjectInterface;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibProduct.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibProduct implements VibObjectInterface, VibFieldableObjectInterface {

  protected $code;
  protected $name;
  protected $vat;

  /**
   * VibProduct constructor.
   *
   * @param $code
   * @param $name
   * @param $vat
   */
  public function __construct($code, $name, $vat) {
    $this->code = $code;
    $this->name = $name;
    $this->vat = $vat;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getVat() {
    return (string) $this->vat;
  }

  /**
   * @return string
   */
  public function getVatPercentage() {
    return (string) ($this->getVat() / 100);
  }

  /**
   * @return string
   */
  public function getVatPercentageLabel() {
    return $this->getVat() . '%';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLabel() {
    return $this->getCode() . ' - ' . $this->getName() . ' (' . $this->getVatPercentageLabel() . ')';
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldKey() {
    return $this->getCode();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#markup' => $this->getCode() . ' - ' . $this->getName() . ' (' . $this->getVatPercentageLabel() . ')',
      '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'code' => $this->getCode(),
      'name' => $this->getName(),
      'vat' => $this->getVat(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['ProductCode'], $json['ProductName'], $json['VAT']);
  }

}
