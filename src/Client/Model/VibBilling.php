<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibBilling.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibBilling implements VibObjectInterface {

  /**
   * VibBilling constructor.
   *
   * @param $address
   * @param $city
   * @param $state
   * @param $country
   * @param $countryName
   * @param $postalCode
   */
  public function __construct(
    protected $address,
    protected $city,
    protected $state,
    protected $country,
    protected $countryName,
    protected $postalCode
  ) {}

  /**
   * @return string
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getCountryName() {
    return $this->countryName;
  }

  /**
   * @return string
   */
  public function getPostalCode() {
    return $this->postalCode;
  }

  /**
   * Returns the postal code.
   *
   * @return string
   *   The postal code.
   *
   * @deprecated PostCode is not an English word, renamed to PostalCode!
   */
  public function getPostCode() {
    return $this->getPostalCode();
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Address'], $json['City'], $json['State'], $json['Country'], $json['CountryName'], $json['PostCode']);
  }

}
