<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\vib_service\Client\Model\VibAddress;
use Drupal\vib_service\Client\Model\VibCentre;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderPayment.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibOrderPayment implements VibObjectInterface {

  protected $centre;
  protected $jobAllocation;
  protected $project;
  protected $transactionId;
  protected $paymentType;

  /**
   * VibOrderPayment constructor.
   *
   * @param \Drupal\vib_service\Client\Model\VibCentre $centre
   * @param \Drupal\vib_commerce\Client\Model\VibJobAllocation $job_allocation
   * @param \Drupal\vib_commerce\Client\Model\VibProject $project
   * @param $transaction_id
   * @param $payment_type
   */
  public function __construct(VibCentre $centre, VibJobAllocation $job_allocation, VibProject $project, $transaction_id, $payment_type) {
    $this->centre = $centre;
    $this->jobAllocation = $job_allocation;
    $this->project = $project;
    $this->transactionId = $transaction_id;
    $this->paymentType = $payment_type;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre
   */
  public function getCentre() {
    return $this->centre;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibJobAllocation
   */
  public function getJobAllocation() {
    return $this->jobAllocation;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibProject
   */
  public function getProject() {
    return $this->project;
  }

  /**
   * @return string
   */
  public function getTransactionId() {
    return $this->transactionId;
  }

  /**
   * @return string
   */
  public function getPaymentType() {
    return $this->paymentType;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $vib_centre = new VibCentre($json['CenterCode'], $json['CenterCode'], NULL, TRUE, [], [], [], VibAddress::createFromJson([]));
    $vib_project = new VibProject($json['ProjectCode'], '', $vib_centre->getCode(), $vib_centre->getDescription());
    $vib_job_allocation = new VibJobAllocation($json['JobAllocationCode'], '', $vib_project->getCode(), $vib_project->getName(), $vib_centre->getCode(), $vib_centre->getDescription());

    return new static(
      $vib_centre,
      $vib_job_allocation,
      $vib_project,
      $json['TransactionId'],
      $json['PaymentType']
    );
  }

}
