<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_price\Price;
use Drupal\vib_service\Client\Model\VibMetaData;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderItemResponse.
 *
 * @package Drupal\vib_commerce\Client\Model
 *
 * This is the object returned when fetching an existing order item.
 */
class VibOrderItemResponse implements VibObjectInterface {

  protected $id;
  protected $shopProductCode;
  protected $shopProductName;
  protected $shopProductDescription;
  protected $productCode;
  protected $productName;
  protected $productCategoryCode;
  protected $productCategoryName;
  protected $tax;
  protected $couponCode;
  protected $couponName;
  protected $quantity;
  protected $price;
  protected $discount;
  protected $priceAfterDiscount;
  protected $shipping;
  protected $subtotal;
  protected $metaData;

  /**
   * VibOrderItemResponse constructor.
   *
   * @param $id
   * @param $shop_product_code
   * @param $shop_product_name
   * @param $shop_product_description
   * @param $product_code
   * @param $product_name
   * @param $product_category_code
   * @param $product_category_name
   * @param \Drupal\vib_commerce\Client\Model\VibTax $tax
   * @param $coupon_code
   * @param $coupon_name
   * @param $quantity
   * @param \Drupal\commerce_price\Price $price
   * @param \Drupal\commerce_price\Price $discount
   * @param \Drupal\commerce_price\Price $price_after_discount
   * @param \Drupal\commerce_price\Price $shipping
   * @param \Drupal\commerce_price\Price $subtotal
   * @param \Drupal\vib_service\Client\Model\VibMetaData $data
   */
  public function __construct($id,
  $shop_product_code,
  $shop_product_name,
  $shop_product_description,
  $product_code,
  $product_name,
                              $product_category_code,
  $product_category_name,
  VibTax $tax,
  $coupon_code,
  $coupon_name,
  $quantity,
  Price $price,
                              Price $discount,
  Price $price_after_discount,
  Price $shipping,
  Price $subtotal,
  VibMetaData $data) {
    $this->id = $id;
    $this->shopProductCode = $shop_product_code;
    $this->shopProductName = $shop_product_name;
    $this->shopProductDescription = $shop_product_description;
    $this->productCode = $product_code;
    $this->productName = $product_name;
    $this->productCategoryCode = $product_category_code;
    $this->productCategoryName = $product_category_name;
    $this->tax = $tax;
    $this->couponCode = $coupon_code;
    $this->couponName = $coupon_name;
    $this->quantity = $quantity;
    $this->price = $price;
    $this->discount = $discount;
    $this->priceAfterDiscount = $price_after_discount;
    $this->shipping = $shipping;
    $this->subtotal = $subtotal;
    $this->metaData = $data;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getShopProductCode() {
    return $this->shopProductCode;
  }

  /**
   * @return string
   */
  public function getShopProductName() {
    return $this->shopProductName;
  }

  /**
   * @return string
   */
  public function getShopProductDescription() {
    return $this->shopProductDescription;
  }

  /**
   * @return string
   */
  public function getProductCode() {
    return $this->productCode;
  }

  /**
   * @return string
   */
  public function getProductName() {
    return $this->productName;
  }

  /**
   * @return string
   */
  public function getProductCategoryCode() {
    return $this->productCategoryCode;
  }

  /**
   * @return string
   */
  public function getProductCategoryName() {
    return $this->productCategoryName;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibTax
   */
  public function getTax() {
    return $this->tax;
  }

  /**
   * @return string
   */
  public function getCouponCode() {
    return $this->couponCode;
  }

  /**
   * @return string
   */
  public function getCouponName() {
    return $this->couponName;
  }

  /**
   * @return int
   */
  public function getQuantity() {
    return $this->quantity;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getPrice() {
    return $this->price;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getDiscount() {
    return $this->discount;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getPriceAfterDiscount() {
    return $this->priceAfterDiscount;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getShipping() {
    return $this->shipping;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getSubtotal() {
    return $this->subtotal;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   */
  public function getMetaData() {
    return $this->metaData;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Id' => $this->getId(),
      'ShopProductCode' => $this->getShopProductCode(),
      'ShopProductName' => $this->getShopProductName(),
      'ShopProductDescription' => $this->getShopProductDescription(),
      'ProductCode' => $this->getProductCode(),
      'ProductName' => $this->getProductName(),
      'ProductCategoryCode' => $this->getProductCategoryCode(),
      'ProductCategoryName' => $this->getProductCategoryName(),
      'Tax' => $this->getTax()->toJson(),
      'CouponCode' => $this->getCouponCode(),
      'CouponName' => $this->getCouponName(),
      'Qty' => $this->getQuantity(),
      'Price' => $this->getPrice()->toArray(),
      'Discount' => $this->getQuantity(),
      'PriceAfterDiscount' => $this->getPriceAfterDiscount()->toArray(),
      'Shipping' => $this->getShipping()->toArray(),
      'SubTotal' => $this->getSubtotal()->toArray(),
      'Metadata' => $this->getMetaData()->toJson(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['Id'],
      $json['ShopProductCode'],
      $json['ShopProductName'],
      $json['ShopProductDescription'],
      $json['ProductCode'],
      $json['ProductName'],
      $json['ProductCategoryCode'],
      $json['ProductCategoryName'],
      new VibTax($json['TaxName'], $json['TaxPercent'], new Price($json['Tax'], 'EUR')),
      $json['CouponCode'],
      $json['CouponName'],
      $json['Qty'],
      new Price($json['Price'], 'EUR'),
      new Price($json['Discount'], 'EUR'),
      new Price($json['PriceAfterDiscount'], 'EUR'),
      new Price($json['Shipping'], 'EUR'),
      new Price($json['SubTotal'], 'EUR'),
      new VibMetaData($json['Metadata'])
    );
  }

}
