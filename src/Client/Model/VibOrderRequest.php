<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\VibClientException;

/**
 * Class VibOrderRequest.
 *
 * @package Drupal\vib_commerce\Client\Model
 *
 * This is the object to be used when trying to send a POST request for creating an order.
 */
class VibOrderRequest implements VibObjectInterface {

  protected $userId;
  protected $shopOrderId;
  protected $orderCancellationBy;
  protected $orderItems;

  /**
   * VibOrder constructor.
   *
   * @param $user_id
   * @param $shop_order_id
   * @param $order_cancellation_by
   * @param array $order_items
   */
  public function __construct($user_id, $shop_order_id, $order_cancellation_by, array $order_items) {
    $this->userId = $user_id;
    $this->shopOrderId = $shop_order_id;
    $this->orderCancellationBy = $order_cancellation_by;
    $this->orderItems = $order_items;
  }

  /**
   * @return string
   */
  public function getUserId() {
    return $this->userId;
  }

  /**
   * @param $id
   */
  public function setUserId($id) {
    $this->userId = $id;
  }

  /**
   * @return string
   */
  public function getShopOrderId() {
    return $this->shopOrderId;
  }

  /**
   * @param $id
   */
  public function setShopOrderId($id) {
    $this->shopOrderId = $id;
  }

  /**
   * @return \DateTimeImmutable
   */
  public function getOrderCancellationBy() {
    return $this->orderCancellationBy;
  }

  /**
   * @param \DateTimeImmutable $cancellation_by
   */
  public function setOrderCancellationBy(\DateTimeImmutable $cancellation_by) {
    $this->orderCancellationBy = $cancellation_by;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderItemRequest[]
   */
  public function getOrderItems() {
    return $this->orderItems;
  }

  /**
   * @param array $order_items
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public function setOrderItems(array $order_items) {
    foreach ($order_items as $order_item) {
      if (!$order_item instanceof VibOrderItemRequest) {
        throw new VibClientException('Order items must be of type "VibOrderItemRequest"');
      }
    }
    $this->orderItems = $order_items;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    $json = [
      'UserId' => $this->getUserId(),
      'ShopOrderId' => $this->getShopOrderId(),
      'OrderCancellationBy' => $this->getOrderCancellationBy()
        ->format(\DateTimeInterface::ATOM),
    ];

    foreach ($this->getOrderItems() as $order_item) {
      $json['OrderItems'][] = $order_item->toJson();
    }

    return $json;
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {

  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param $payment_gateway_configuration
   * @return \Drupal\vib_commerce\Client\Model\VibOrderRequest
   * @throws \Exception
   */
  public static function createFromCommerceOrder(OrderInterface $order, $payment_gateway_configuration) {
    $order_items = [];
    /**
     * @var \Drupal\vib_service_authorization\VibServiceUserManager $user_manager
     */
    $user_manager = \Drupal::service('vib_service_authorization.user_manager');

    foreach ($order->getItems() as $item) {
      $order_items[] = VibOrderItemRequest::createFromCommerceOrderItem($item);
    }

    if (!$order_cancellation = (new \DateTimeImmutable('now'))->add(new \DateInterval('P' . $payment_gateway_configuration['order_cancellation_by'] . 'D'))) {
      throw new VibClientException('"order_cancellation_by" is empty');
    }

    if (!$vib_uid = $user_manager->getVibServiceExternalUid($order->getCustomer())) {
      throw new VibClientException('Could not fetch VIB uid');
    }

    return new static($vib_uid ?? 0, $order->id(), $order_cancellation, $order_items);
  }

}
