<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibShipping.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibShipping implements VibObjectInterface {

  protected $address;
  protected $city;
  protected $state;
  protected $country;
  protected $countryName;
  protected $postCode;

  /**
   * VibBilling constructor.
   *
   * @param $address
   * @param $city
   * @param $state
   * @param $country
   * @param $country_name
   * @param $post_code
   */
  public function __construct($address, $city, $state, $country, $country_name, $post_code) {
    $this->address = $address;
    $this->city = $city;
    $this->state = $state;
    $this->country = $country;
    $this->countryName = $country_name;
    $this->postCode = $post_code;
  }

  /**
   * @return string
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getCountryName() {
    return $this->countryName;
  }

  /**
   * @return string
   */
  public function getPostCode() {
    return $this->postCode;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Address'], $json['City'], $json['State'], $json['Country'], $json['CountryName'], $json['PostCode']);
  }

}
