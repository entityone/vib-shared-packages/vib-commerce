<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_price\Price;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderAmount.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibOrderAmount implements VibObjectInterface {

  protected $subtotal;
  protected $shippingTotal;
  protected $taxTotal;
  protected $total;

  /**
   * VibOrderAmount constructor.
   *
   * @param \Drupal\commerce_price\Price $subtotal
   * @param \Drupal\commerce_price\Price $shipping_total
   * @param \Drupal\commerce_price\Price $tax_total
   * @param \Drupal\commerce_price\Price $total
   */
  public function __construct(Price $subtotal, Price $shipping_total, Price $tax_total, Price $total) {
    $this->subtotal = $subtotal;
    $this->shippingTotal = $shipping_total;
    $this->taxTotal = $tax_total;
    $this->total = $total;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getSubtotal() {
    return $this->subtotal;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getShippingTotal() {
    return $this->shippingTotal;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getTaxTotal() {
    return $this->taxTotal;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getTotal() {
    return $this->total;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      new Price($json['SubTotal'], 'EUR'),
      new Price($json['ShippingTotal'], 'EUR'),
      new Price($json['TaxTotal'], 'EUR'),
      new Price($json['Total'], 'EUR')
    );
  }

}
