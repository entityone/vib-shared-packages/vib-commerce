<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_price\Price;
use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibOrderRefundItem.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibOrderRefundItem implements VibObjectInterface {

  protected $id;
  protected $refundAmount;
  protected $orderItem;

  /**
   * VibOrderRefundItem constructor.
   *
   * @param $id
   * @param \Drupal\commerce_price\Price $refundAmount
   * @param \Drupal\vib_commerce\Client\Model\VibOrderItemResponse $order_item
   */
  public function __construct($id, Price $refundAmount, VibOrderItemResponse $order_item) {
    $this->id = $id;
    $this->refundAmount = $refundAmount;
    $this->orderItem = $order_item;
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return \Drupal\commerce_price\Price
   */
  public function getRefundAmount() {
    return $this->refundAmount;
  }

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibOrderItemResponse
   */
  public function getOrderItem() {
    return $this->orderItem;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Id' => $this->getId(),
      'RefundAmount' => $this->getRefundAmount()->toArray(),
      'OrderItem' => $this->getOrderItem()->toJson(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $order_item_json = $json;
    $order_item_json['Id'] = $order_item_json['OrderItemId'];
    $order_item = VibOrderItemResponse::createFromJson($order_item_json);

    return new static(
      $json['Id'],
      new Price($json['RefundAmount'], 'EUR'),
      $order_item
    );
  }

}
