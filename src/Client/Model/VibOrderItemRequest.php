<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\vib_service\Client\Model\VibMetaData;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\VibClientException;

/**
 * This is the object to be used when trying to send a POST request for creating an order(item).
 */
class VibOrderItemRequest implements VibObjectInterface {

  protected ?VibMetaData $metadata = NULL;
  protected ?string $itemGroupName = NULL;
  protected ?VibJobAllocation $jobAllocation = NULL;
  protected ?VibProject $project = NULL;

  public function __construct(
    protected ?string $code,
    protected ?string $name,
    protected ?string $description,
    protected ?VibProduct $product,
    protected ?Price $price,
    protected null|int|string $quantity,
    protected ?Price $discount,
    protected ?Price $shipping,
    protected ?string $discountDescription,
  ) {}

  public function getCode(): ?string {
    return $this->code;
  }

  public function setCode(string $code): static {
    $this->code = $code;
    return $this;
  }

  public function getName(): ?string {
    return $this->name;
  }

  public function setName($name): static {
    $this->name = $name;
    return $this;
  }

  public function getDescription(): ?string {
    return $this->description;
  }

  public function setDescription($description): static {
    $this->description = $description;
    return $this;
  }

  public function getProduct(): ?VibProduct {
    return $this->product;
  }

  public function setProduct(VibProduct $product): static {
    $this->product = $product;
    return $this;
  }

  public function getPrice(): ?Price {
    return $this->price;
  }

  public function setPrice(Price $price): static {
    $this->price = $price;
    return $this;
  }

  public function getDiscount(): ?Price {
    return $this->discount;
  }

  public function setDiscount(Price $discount): static {
    $this->discount = $discount;
    return $this;
  }

  public function getDiscountDescription(): ?string {
    return $this->discountDescription;
  }

  public function setDiscountDescription(string $discount_description): static {
    $this->discountDescription = $discount_description;
    return $this;
  }

  public function getShipping(): ?Price {
    return $this->shipping;
  }

  public function setShipping(Price $shipping): static {
    $this->shipping = $shipping;
    return $this;
  }

  public function getQuantity(): null|int|string {
    return $this->quantity;
  }

  public function setQuantity(int|string $quantity): static {
    $this->quantity = $quantity;
    return $this;
  }

  public function getJobAllocation(): ?VibJobAllocation {
    return $this->jobAllocation;
  }

  public function setJobAllocation(VibJobAllocation $job): static {
    $this->jobAllocation = $job;
    return $this;
  }

  public function getProject(): ?VibProject {
    return $this->project;
  }

  public function setProject(VibProject $project): static {
    $this->project = $project;
    return $this;
  }

  public function getItemGroupName(): ?string {
    return $this->itemGroupName;
  }

  public function setItemGroupName(string $group_name): static {
    $this->itemGroupName = $group_name;
    return $this;
  }

  public function getMetaData(): ?VibMetaData {
    return $this->metadata;
  }

  public function setMetaData(VibMetaData $data): static {
    $this->metadata = $data;
    return $this;
  }

  public function toJson(): array {
    $json = [
      'ItemCode' => $this->getCode(),
      'ItemName' => $this->getName(),
      'ItemDescription' => $this->getDescription(),
      'ItemProductCode' => $this->getProduct()->getCode(),
      'ItemPrice' => $this->getPrice()->getNumber(),
      'ItemDiscount' => $this->getDiscount()->getNumber(),
      'ItemDiscountDescription' => $this->getDiscountDescription(),
      'ItemShipping' => $this->getShipping()->getNumber(),
      'ItemQty' => $this->getQuantity(),
      'Metadata' => new \stdClass(),
    ];

    // Add the project code from the configured project if it exists.
    if ($project = $this->getProject()) {
      $json['ProjectCode'] = $project->getCode();
    }
    // Otherwise, for legacy reasons, add the existing job allocation and
    // project codes.
    elseif ($job_allocation = $this->getJobAllocation()) {
      $json['JobAllocationCode'] = $job_allocation->getCode();
      $json['ProjectCode'] = $job_allocation->getProjectCode();
    }

    if ($item_group_name = $this->getItemGroupName()) {
      $json['ItemGroupName'] = $item_group_name;
    }

    if ($meta_data = $this->getMetaData()) {
      $json['Metadata'] = $meta_data->toJson();
    }

    return $json;
  }

  public static function createFromJson(array $json): static {
    return new static(
      $json['ItemCode'],
      $json['ItemName'],
      $json['ItemDescription'],
      $json['ItemProduct'],
      new Price($json['ItemPrice']),
      $json['ItemQty'],
      new Price($json['ItemDiscount']),
      new Price($json['ItemShipping']),
      $json['ItemDiscountDescription'],
    );
  }

  public static function createFromCommerceOrderItem(OrderItemInterface $item): static {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $item->getPurchasedEntity();

    if ($variation->hasField('vib_vat_product') && $variation->vib_vat_product->isEmpty()) {
      throw new VibClientException('No VIB product for product variation "' . $variation->label() . '"" (' . $variation->id() . ')');
    }

    /** @var \Drupal\vib_commerce\Client\Model\VibProduct $vib_product */
    $vib_product = $variation->get('vib_vat_product')
      ->first()
      ->getVibObject();
    $vib_order_item = new static(
      $variation->getSku(),
      $item->getTitle(),
      NULL,
      $vib_product,
      $item->getUnitPrice(),
      $item->getQuantity(),
      new Price(0, 'EUR'),
      new Price(0, 'EUR'),
      NULL
    );

    if ($product_type = ProductType::load($variation->getProduct()?->bundle())) {
      if ($project_code_field = $product_type->getThirdPartySetting('vib_commerce', 'vib_project_code_field')) {
        $product = $variation->getProduct();
        if ($product->hasField($project_code_field)
          && ($project = $product->get($project_code_field)
            ->first()?->getVibObject())) {
          // Attach the project to the order item.
          $vib_order_item->setProject($project);
        }
      }

      if ($job_allocation_field = $product_type->getThirdPartySetting('vib_commerce', 'vib_job_allocation_field')) {
        $product = $variation->getProduct();
        if ($product->hasField($job_allocation_field)
          && ($job_allocation = $product->get($job_allocation_field)
            ->first()?->getVibObject())) {
          // Attach the job allocation code to the order item.
          $vib_order_item->setJobAllocation($job_allocation);
        }
      }
    }

    return $vib_order_item;
  }

}
