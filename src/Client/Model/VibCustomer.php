<?php

namespace Drupal\vib_commerce\Client\Model;

use Drupal\vib_service\Client\Model\VibObjectInterface;

/**
 * Class VibCustomer.
 *
 * @package Drupal\vib_commerce\Client\Model
 */
class VibCustomer implements VibObjectInterface {

  protected $email;
  protected $firstName;
  protected $lastName;
  protected $organisation;
  protected $vatNumber;
  protected $phone;

  /**
   * VibCustomer constructor.
   *
   * @param $email
   * @param $first_name
   * @param $last_name
   * @param $organisation
   * @param $vat_number
   * @param $phone
   */
  public function __construct($email, $first_name, $last_name, $organisation, $vat_number, $phone) {
    $this->email = $email;
    $this->firstName = $first_name;
    $this->lastName = $last_name;
    $this->organisation = $organisation;
    $this->vatNumber = $vat_number;
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @return string
   */
  public function getOrganisation() {
    return $this->organisation;
  }

  /**
   * @return string
   */
  public function getVatNumber() {
    return $this->vatNumber;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {

  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Email'], $json['FirstName'], $json['LastName'], $json['Organisation'], $json['VATNumber'], $json['Phone']);
  }

}
