<?php

namespace Drupal\vib_commerce\Client;

/**
 * Class VibOrderStatuses.
 *
 * @package Drupal\vib_commerce\Client
 */
final class VibOrderStatuses {

  /**
   * Maps order status to payment status.
   *
   * @param string $vib_order_status
   *   The order status.
   *
   * @return string
   *   The payment status.
   */
  public static function mapToPaymentStatus($vib_order_status) {
    // @TODO: Check refund statuses.
    $mapping = [
      'new' => 'authorization',
      'initialized' => 'authorization',
      'declined' => 'authorization_voided',
      'cancelled' => 'authorization_voided',
      'completed' => 'completed',
      'expired' => 'authorization_expired',
      'uncleared' => 'authorization_voided',
      'void' => 'authorization_voided',
      'partial_refunded' => 'partially_refunded',
      'refunded' => 'refunded',
      'reserved' => 'authorization',
      'chargedback' => 'authorization_voided',
    ];

    return isset($mapping[$vib_order_status]) ? $mapping[$vib_order_status] : FALSE;
  }

}
