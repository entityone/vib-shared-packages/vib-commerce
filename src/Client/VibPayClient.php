<?php

namespace Drupal\vib_commerce\Client;

use Drupal\Component\Serialization\Json;
use Drupal\vib_commerce\Client\Model\VibJobAllocation;
use Drupal\vib_commerce\Client\Model\VibOrderRefund;
use Drupal\vib_commerce\Client\Model\VibOrderRequest;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\vib_commerce\Client\Model\VibOrderSummary;
use Drupal\vib_commerce\Client\Model\VibProduct;
use Drupal\vib_commerce\Client\Model\VibProject;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service\Client\VibServiceClient;
use GuzzleHttp\RequestOptions;

/**
 * Class VibPayClient.
 *
 * @package Drupal\vib_commerce\Client
 */
class VibPayClient extends VibServiceClient implements VibPayClientInterface {

  /**
   * {@inheritdoc}
   */
  public function apiBearerToken($scope = 'orders roles users codes') {
    return parent::apiBearerToken('orders roles products codes');
  }

  /**
   * {@inheritdoc}
   */
  public function addOrder(VibOrderRequest $order) {
    $options = [
      'headers' => ['Content-Type' => 'application/json'],
      RequestOptions::BODY => Json::encode($order->toJson()),
    ];

    $content = $this->request('POST', 'orders', $options);

    if (empty($content['Success'])) {
      $message = NULL;
      foreach ($content['Errors'] as $error_code) {
        $message .= VibOrderErrorCodes::getMessage($error_code) . ', ';
      }
      throw new VibClientException($message);
    }

    return VibOrderSummary::createFromJson($content);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder($order_id) {
    $json = $this->request('GET', 'orders/' . $order_id);
    return VibOrderResponse::createFromJson($json);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderPdf($order_id) {
    $response = $this->request('GET', 'orders/' . $order_id . '/pdf', [], FALSE);
    return $response->getBody();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderRefunds($order_id) {
    $refunds = [];
    $json = $this->request('GET', 'orders/' . $order_id . '/refunds');

    foreach ($json as $refund) {
      $refunds[] = VibOrderRefund::createFromJson($refund);
    }

    return $refunds;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetProducts() {
    $products = [];

    $json = $this->request('GET', 'products');

    foreach ($json as $product) {
      $products[] = VibProduct::createFromJson($product);
    }

    return $products;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetProjects() {
    $projects = [];

    $json = $this->request('GET', 'projects');

    foreach ($json as $project) {
      $projects[] = VibProject::createFromJson($project);
    }

    return $projects;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetJobAllocations(array $projects = NULL) {
    $job_allocations = [];
    $query = [];
    /** @var \Drupal\vib_commerce\Client\Model\VibProject $project */
    foreach ($projects as $project) {
      $query['projectCodes'][] = $project->getCode();
    }

    $options = [
      'query' => $query,
    ];

    $json = $this->request('GET', 'jobs', $options);

    foreach ($json as $role) {
      $job_allocations[] = VibJobAllocation::createFromJson($role);
    }

    return $job_allocations;
  }

}
