<?php

namespace Drupal\vib_commerce\Client;

use Drupal\vib_commerce\Client\Model\VibOrderRequest;
use Drupal\vib_service\Client\VibServiceClientInterface;

/**
 * Interface VibPayClientInterface.
 *
 * @package Drupal\vib_commerce\Client
 */
interface VibPayClientInterface extends VibServiceClientInterface {

  /**
   * @param \Drupal\vib_commerce\Client\Model\VibOrderRequest $order
   * @return \Drupal\vib_commerce\Client\Model\VibOrderSummary
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function addOrder(VibOrderRequest $order);

  /**
   * @param $order_id
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getOrder($order_id);

  /**
   * Returns a stream. Can be saved as pdf by using file_put_contents().
   *
   * @param $order_id
   *
   * @return \Psr\Http\Message\StreamInterface
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getOrderPdf($order_id);

  /**
   * @param $order_id
   * @return \Drupal\vib_commerce\Client\Model\VibOrderRefund[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getOrderRefunds($order_id);

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibProduct[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetProducts();

  /**
   * @return \Drupal\vib_commerce\Client\Model\VibProject[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetProjects();

  /**
   * @param array|null $projects
   *
   * @return \Drupal\vib_commerce\Client\Model\VibJobAllocation[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetJobAllocations(array $projects = NULL);

}
