<?php

namespace Drupal\vib_commerce\Entity;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\vib_commerce\Client\Model\VibOrderRefund;

/**
 * Provides an interface for defining Refund entities.
 *
 * @ingroup vib_commerce
 */
interface RefundInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Returns the remote id.
   *
   * @return string
   *   The remote id.
   */
  public function getRemoteId();

  /**
   * Returns the remote url to the order refunds page.
   *
   * @return \Drupal\Core\Url
   *   The url to the order refunds page.
   */
  public function getRemoteUrl();

  /**
   * Sets the remote id.
   *
   * @param string $id
   *   The remote id.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setRemoteId($id);

  /**
   * Sets the items.
   *
   * @param string[] $items
   *   A list of items.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setItems(array $items);

  /**
   * Returns the refund amount.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getAmount();

  /**
   * Sets the amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setAmount(Price $amount);

  /**
   * Returns the refund subtotal.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getSubTotal();

  /**
   * Sets the subtotal.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setSubTotal(Price $amount);

  /**
   * Returns the refund tax.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getTax();

  /**
   * Sets the tax amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setTax(Price $amount);

  /**
   * Returns the admin fee.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getAdminFee();

  /**
   * Sets the refund admin fee.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setAdminFee(Price $amount);

  /**
   * Returns the refund total.
   *
   * @return \Drupal\commerce_price\Price
   *   The price.
   */
  public function getTotal();

  /**
   * Sets the total refund amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setTotal(Price $amount);

  /**
   * Sets a comment.
   *
   * @param string $comment
   *   The comment.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setComment($comment);

  /**
   * Returns the order the refund belongs to.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder();

  /**
   * Sets the order the refund belongs to.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setOrder(OrderInterface $order);

  /**
   * Returns the refund API object.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderRefund
   *   The refund API object.
   */
  public function getRefundApiObject();

  /**
   * Sets the refund object fetched from the API.
   *
   * @param \Drupal\vib_commerce\Client\Model\VibOrderRefund $refund
   *   The refund object fetched from the API.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The entity.
   */
  public function setRefundApiObject(VibOrderRefund $refund);

  /**
   * Gets the Refund creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Refund.
   */
  public function getCreatedTime();

  /**
   * Sets the Refund creation timestamp.
   *
   * @param int $timestamp
   *   The Refund creation timestamp.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The called Refund entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Syncs Refund entity with API object.
   *
   * @param \Drupal\vib_commerce\Client\Model\VibOrderRefund $api_object
   *   The api object.
   * @param \Drupal\vib_commerce\Entity\RefundInterface $refund
   *   The refund entity.
   */
  public static function syncWithApi(VibOrderRefund $api_object, RefundInterface $refund);

}
