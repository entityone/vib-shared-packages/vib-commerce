<?php

namespace Drupal\vib_commerce\Entity;

use Drupal\views\EntityViewsData;

/**
 * Class RefundViewsData.
 *
 * @package Drupal\vib_commerce\Entity
 */
class RefundViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['vib_refund']['remote_id_linked'] = [
      'title' => $this->t('Remote ID (linked)'),
      'field' => [
        'title' => $this->t('Remote ID (linked)'),
        'id' => 'vib_commerce_remote_id_linked',
      ],
    ];

    $data['views']['vib_refunds_total'] = [
      'title' => t('Refunds total'),
      'help' => t('Displays the refunds total field, requires an Order ID argument.'),
      'area' => [
        'id' => 'vib_refunds_total',
      ],
    ];

    return $data;
  }

}
