<?php

namespace Drupal\vib_commerce\Entity;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\vib_commerce\Client\Model\VibOrderRefund;
use Drupal\vib_commerce\Client\Model\VibOrderRefundItem;
use Drupal\vib_service_authorization\Plugin\OpenIDConnectClient\OpenIDConnectVibServiceClient;

/**
 * Defines the Refund entity.
 *
 * @ingroup vib_commerce
 *
 * @ContentEntityType(
 *   id = "vib_refund",
 *   label = @Translation("Refund"),
 *   handlers = {
 *     "views_data" = "Drupal\vib_commerce\Entity\RefundViewsData",
 *     "form" = {
 *     },
 *     "access" = "Drupal\vib_commerce\RefundAccessControlHandler",
 *     "storage" = "Drupal\vib_commerce\RefundStorage",
 *   },
 *   base_table = "vib_refund",
 *   admin_permission = "administer refund entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vib_refund/{vib_refund}",
 *     "add-form" = "/admin/structure/vib_refund/add",
 *     "edit-form" = "/admin/structure/vib_refund/{vib_refund}/edit",
 *     "delete-form" = "/admin/structure/vib_refund/{vib_refund}/delete",
 *     "collection" = "/admin/structure/vib_refund",
 *   },
 *   field_ui_base_route = "vib_refund.settings"
 * )
 */
class Refund extends ContentEntityBase implements RefundInterface {

  use EntityChangedTrait;

  /**
   * The refund API object.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderRefund
   */
  protected $apiObject;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return 'Refund for ' . $this->getTotal()
        ->getCurrencyCode() . $this->getTotal()->getNumber();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUrl() {
    $client = OpenIDConnectVibServiceClient::createFromState();
    $vib_order = $this->getOrder()->getData('vib_pay')['vib_order_id'];
    return Url::fromUri($client->getConfiguration()['base_uri'] . '/admin/#/order/' . $vib_order . '/cancellation');
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($id) {
    $this->set('remote_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setItems(array $items) {
    $this->set('items', array_map(function ($item) {
      return ['value' => $item];
    }, $items));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    if (!$this->get('amount')->isEmpty()) {
      return $this->get('amount')->first()->toPrice();
    }

    return new Price('0', 'EUR');
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount(Price $amount) {
    $this->set('amount', $amount->toArray());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdminFee() {
    if (!$this->get('admin_fee')->isEmpty()) {
      return $this->get('admin_fee')->first()->toPrice();
    }

    return new Price('0', 'EUR');
  }

  /**
   * {@inheritdoc}
   */
  public function setAdminFee(Price $amount) {
    $this->set('admin_fee', $amount->toArray());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubTotal() {
    if (!$this->get('subtotal')->isEmpty()) {
      return $this->get('subtotal')->first()->toPrice();
    }

    return new Price('0', 'EUR');
  }

  /**
   * {@inheritdoc}
   */
  public function setSubTotal(Price $amount) {
    $this->set('subtotal', $amount->toArray());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTax() {
    if (!$this->get('tax')->isEmpty()) {
      return $this->get('tax')->first()->toPrice();
    }

    return new Price('0', 'EUR');
  }

  /**
   * {@inheritdoc}
   */
  public function setTax(Price $amount) {
    $this->set('tax', $amount->toArray());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotal() {
    if (!$this->get('total')->isEmpty()) {
      return $this->get('total')->first()->toPrice();
    }

    return new Price('0', 'EUR');
  }

  /**
   * {@inheritdoc}
   */
  public function setTotal(Price $amount) {
    $this->set('total', $amount->toArray());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setComment($comment) {
    $this->set('comment', $comment);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(OrderInterface $order) {
    $this->set('order', $order->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefundApiObject() {
    return $this->apiObject;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefundApiObject(VibOrderRefund $refund) {
    $this->apiObject = $refund;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Refund entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Amount'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tax'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Tax'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['subtotal'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Subtotal'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['admin_fee'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Admin fee'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Total'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['items'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Items'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['order'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function syncWithApi(VibOrderRefund $api_object, RefundInterface $refund) {
    $refund->setComment($api_object->getComment());
    $refund->setItems(array_map(function (VibOrderRefundItem $item) {
      return $item->getOrderItem()
          ->getProductName() . ' (' . $item->getRefundAmount() . ')';
    }, $api_object->getItems()));
    $refund->setAmount($api_object->getAmount());
    $refund->setTax($api_object->getTaxTotal());
    $refund->setSubTotal($api_object->getSubTotal());
    $refund->setAdminFee($api_object->getAdminFee());
    $refund->setTotal($api_object->getTotal());
  }

}
