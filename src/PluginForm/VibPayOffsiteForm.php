<?php

namespace Drupal\vib_commerce\PluginForm;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vib_commerce\Client\Model\VibOrderRequest;
use Drupal\vib_commerce\Client\Model\VibOrderSummary;
use Drupal\vib_commerce\Client\VibPayClientInterface;
use Drupal\vib_commerce\Event\VibCommerceEvents;
use Drupal\vib_commerce\Event\VibOrderRequestEvent;
use Drupal\vib_service\Client\VibClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class VibPayOffsiteForm.
 *
 * @package Drupal\vib_commerce\PluginForm
 */
class VibPayOffsiteForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The vib client.
   *
   * @var \Drupal\vib_commerce\Client\VibPayClientInterface
   */
  protected $client;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * VibPayOffsiteForm constructor.
   *
   * @param \Drupal\vib_commerce\Client\VibPayClientInterface $client
   *   The vib client.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(VibPayClientInterface $client, EventDispatcherInterface $event_dispatcher) {
    $this->client = $client;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vib_commerce.pay_client'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\vib_commerce\Plugin\Commerce\PaymentGateway\VibPay $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $extra = [
      'return_url' => $form['#return_url'],
      'cancel_url' => $form['#cancel_url'],
      'capture' => $form['#capture'],
    ];

    $order = $payment->getOrder();
    // Send order to VIB pay.
    try {
      $vib_order = VibOrderRequest::createFromCommerceOrder($order, $payment_gateway_plugin->getConfiguration());
      // Allow other modules to alter vib order before sending to VIB services.
      $event = new VibOrderRequestEvent($order, $vib_order);
      $this->eventDispatcher->dispatch($event, VibCommerceEvents::VIB_ORDER_PRE_SEND);
      // Create new order.
      $vib_order_summary = $this->client->addOrder($vib_order);
    }
    catch (VibClientException $e) {
      throw new PaymentGatewayException('[VIB Service]: ' . $e->getMessage());
    }

    $order = $payment->getOrder();
    // Expand the payment with data retrieved from VIB Services.
    $this->expandPayment($payment, $order, $vib_order_summary);

    $order->setData('vib_pay', [
      'vib_order_id' => $vib_order_summary->getOrderId(),
      'redirect_url' => $vib_order_summary->getRedirectUrl()->toString(),
      'capture' => $extra['capture'],
      // This flag will be set to false when user gets redirected
      // from payment service.
      // This way users cannot execute this callback manually.
      'awaiting_redirect' => TRUE,
    ]);
    $order->save();

    // Save the payment before redirecting to VIB services.
    $payment->save();
    return $this->buildRedirectForm($form, $form_state, $vib_order_summary->getRedirectUrl()
      ->toString(), []);
  }

  /**
   * Expends payment with extra data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderSummary $vib_order
   *   The VIB order.
   */
  protected function expandPayment(PaymentInterface $payment, OrderInterface $order, VibOrderSummary $vib_order) {
    $payment->setRemoteId($vib_order->getOrderId());
    $payment->setRemoteState('new');
  }

}
