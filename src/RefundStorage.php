<?php

namespace Drupal\vib_commerce;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Class RefundStorage.
 *
 * @package Drupal\vib_events
 */
class RefundStorage extends SqlContentEntityStorage implements RefundStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByRemoteId($remote_id) {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('remote_id', $remote_id);

    $result = $query->execute();
    return $result ? $this->load(reset($result)) : NULL;
  }

}
