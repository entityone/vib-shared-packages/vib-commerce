<?php

namespace Drupal\vib_commerce;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Refund entity.
 *
 * @see \Drupal\vib_commerce\Entity\Refund.
 */
class RefundAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\vib_commerce\Entity\RefundInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view published refund entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
