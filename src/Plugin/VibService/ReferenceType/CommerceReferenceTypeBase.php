<?php

namespace Drupal\vib_commerce\Plugin\VibService\ReferenceType;

use Drupal\vib_commerce\Client\VibPayClientInterface;
use Drupal\vib_service\Plugin\VibService\ReferenceTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommerceReferenceTypeBase.
 *
 * @package Drupal\vib_commerce\Plugin\VibService\ReferenceType
 */
abstract class CommerceReferenceTypeBase extends ReferenceTypeBase {

  protected $client;

  /**
   * ProductReferenceType constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\vib_commerce\Client\VibPayClientInterface $client
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VibPayClientInterface $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vib_commerce.pay_client')
    );
  }

}
