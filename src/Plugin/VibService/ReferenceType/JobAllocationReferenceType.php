<?php

namespace Drupal\vib_commerce\Plugin\VibService\ReferenceType;

use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\Entity\Store;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\vib_commerce\Client\VibPayClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ReferenceType(
 *   id = "job_allocation",
 *   label = @Translation("Job allocation"),
 * )
 */
class JobAllocationReferenceType extends CommerceReferenceTypeBase {

  protected $currentStore;

  /**
   * JobAllocationReferenceType constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\vib_commerce\Client\VibPayClientInterface $client
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VibPayClientInterface $client, CurrentStoreInterface $current_store) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $client);
    $this->currentStore = $current_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vib_commerce.pay_client'),
      $container->get('commerce_store.current_store')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function doGetApiItems() {
    // Only fetch allocation codes for the projects configured on the current store.
    if (!$store = $this->currentStore->getStore()) {
      // Use random store.
      $stores = Store::loadMultiple();
      $store = reset($stores);
    }
    if (!$projects = $store->vib_project->referencedVibObjects()) {
      throw new PluginException('No VIB Projects configured on store "' . $store->label() . '"');
    }
    return $this->client->apiGetJobAllocations($projects);
  }

}
