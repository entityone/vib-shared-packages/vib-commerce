<?php

namespace Drupal\vib_commerce\Plugin\VibService\ReferenceType;

/**
 * @ReferenceType(
 *   id = "product",
 *   label = @Translation("Product"),
 * )
 */
class ProductReferenceType extends CommerceReferenceTypeBase {

  /**
   * {@inheritdoc}
   */
  public function doGetApiItems() {
    return $this->client->apiGetProducts();
  }

}
