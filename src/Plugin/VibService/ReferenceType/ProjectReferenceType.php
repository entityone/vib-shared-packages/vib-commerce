<?php

namespace Drupal\vib_commerce\Plugin\VibService\ReferenceType;

/**
 * @ReferenceType(
 *   id = "project",
 *   label = @Translation("Project"),
 * )
 */
class ProjectReferenceType extends CommerceReferenceTypeBase {

  /**
   * {@inheritdoc}
   */
  public function doGetApiItems() {
    return $this->client->apiGetProjects();
  }

}
