<?php

namespace Drupal\vib_commerce\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\vib_commerce\Client\VibOrderStatuses;
use Drupal\vib_commerce\Client\VibPaymentFailureErrorCodes;
use Drupal\vib_commerce\Event\ProfileSyncEvent;
use Drupal\vib_commerce\Event\VibCommerceEvents;
use Drupal\vib_commerce\Event\VibOrderPaymentRefunded;
use Drupal\vib_commerce\Event\VibOrderResponseEvent;
use Drupal\vib_service\Client\VibClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @CommercePaymentGateway(
 *   id = "vib_pay",
 *   label = @Translation("VIB Pay"),
 *   display_label = @Translation("VIB Pay"),
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\vib_commerce\PluginForm\VibPayOffsiteForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class VibPay extends OffsitePaymentGatewayBase {

  protected $vibPayClient;
  protected $vibOrder;
  protected $countryManager;
  protected $eventDispatcher;
  protected $logger;
  /** @var \Drupal\vib_commerce\VibCommerceHelper $vibCommerceHelper */
  protected $vibCommerceHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->vibPayClient = $container->get('vib_commerce.pay_client');
    $instance->countryManager = $container->get('country_manager');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    $instance->logger = $container->get('vib_service.datadog_log');
    $instance->vibCommerceHelper = $container->get('vib_commerce.helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'update_billing_profile' => TRUE,
        'order_cancellation_by' => 14,
        'enable_logging' => FALSE,
        'update_vat' => FALSE,
        'add_refunds_entities' => FALSE,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $url = Url::fromRoute('vib_service.settings')->toString();

    try {
      $this->vibPayClient->apiGetRoles();
    } catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('<a href=":url">Configure</a> OpenID Connect VIB Service settings before proceeding.', [
          ':url' => $url,
        ]));
    }

    $form['credentials'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['form-item'],
      ],
      '#value' => '<strong>' . $this->t('Client ID and Client secret from <a href=":url" >VIB Service OpenID Connect</a> are used.', [
          ':url' => $url,
        ]) . '</strong>',
      '#weight' => 20,
    ];

    $form['update_billing_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update the billing customer profile with address information the customer enters at VIB Service checkout.'),
      '#default_value' => $this->configuration['update_billing_profile'],
    ];

    $form['update_vat'] = [
      '#type' => 'checkbox',
      '#title' => t('Update the VAT based on the calculated VAT in VIB Services'),
      '#description' => $this->t('VAT is recalculated in VIB Services which causes an error margin of 1ct in rare cases.
      This feature updates the VAT amount of the order when it differs from the VAT calculated in VIB services.'),
      '#default_value' => $this->configuration['update_vat'],
    ];

    $form['order_cancellation_by'] = [
      '#type' => 'number',
      '#title' => $this->t('Order cancellation by'),
      '#default_value' => $this->configuration['order_cancellation_by'],
      '#description' => $this->t('Number of days by which an order cancellation is allowed after placing the order.'),
      '#required' => TRUE,
    ];

    $form['add_refunds_entities'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track refunds initialized in VB services.'),
      '#description' => $this->t('Enabling this option will keep a detailed log per order about the refunds history.'),
      '#default_value' => $this->configuration['add_refunds_entities'],
    ];

    $datadog_enabled = \Drupal::config('vib_service.settings')
      ->get('datadog_log');
    $form['enable_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep a detailed log for every order.'),
      '#description' => $datadog_enabled ? $this->t('The logs are stored in datadog, an external logger.') : $this->t('<a href=":url">Enable</a> datadog to use this feature.', [
        ':url' => Url::fromRoute('vib_service.settings')->toString()
      ]),
      '#default_value' => $this->configuration['enable_logging'],
      '#disabled' => !$datadog_enabled
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['update_billing_profile'] = $values['update_billing_profile'];
      $this->configuration['update_vat'] = $values['update_vat'];
      $this->configuration['order_cancellation_by'] = $values['order_cancellation_by'];
      $this->configuration['enable_logging'] = $values['enable_logging'];
      $this->configuration['add_refunds_entities'] = $values['add_refunds_entities'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $body = Json::decode($request->getContent());
    if (empty($body['ShopOrderId']) || empty($body['OrderId'])) {
      $this->log('OrderId or ShopOrderId are empty.');
      throw new PaymentGatewayException('OrderId and ShopOrderId POST params are required.');
    }

    $vib_order_id = $body['OrderId'];
    $order_id = $body['ShopOrderId'];
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = Order::load($order_id);
    $order_data = $order->getData('vib_pay');

    $this->log('Started payment notification.', $order);

    $vib_order = $this->getVibOrder($order, VibOrderResponseEvent::ON_NOTIFY);
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    if ($payment = $payment_storage->loadByRemoteId($order_data['vib_order_id'])) {
      if ($payment->isCompleted() && $vib_order->paymentIsCompleted()) {
        $this->log('Payment was already completed. No need to process notification.', $order);
        return;
      }

      if ($payment->isCompleted() && $vib_order->paymentIsRefunded()) {
        // There is a payment marked as completed, but the vib order payment was refunded.
        // This can occur when someone refunds directly in MSP and not via VIB services.
        // Dispatch event so other modules can take the appropriate action.
        $event = new VibOrderPaymentRefunded($order, $payment, $vib_order);
        $this->eventDispatcher->dispatch($event, VibCommerceEvents::VIB_ORDER_PAYMENT_REFUNDED);

        $this->log('Payment was completed, but VIB order got refunded.', $order);
        return;
      }

    }

    if (empty($order_data['vib_order_id'])) {
      $this->log('VIB order ID not found on Commerce order.', $order);
      throw new PaymentGatewayException('VIB order ID not found on Commerce order.');
    }

    $needs_order_save = FALSE;

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment ?: $this->buildPayment($order, $vib_order);
    $payment->setRemoteState($vib_order->getPaymentStatus());
    // Payment is created before redirecting to VIB services, which means the amount is determined in Drupal.
    // Sometimes the amount can change based on the billing address in VIB services,
    // We have to make sure the VIB services amount is used, that's why we update it here.
    $payment->setAmount($vib_order->getAmount()->getTotal());

    if ($order_data['vib_order_id'] != $vib_order_id) {
      $this->log('VIB order ID (' . $vib_order_id . ') does not match the order ID stored on the Commerce order (' . $order_data['vib_order_id'] . ').', $order);
      // This is probably on old (expired) payment. Just update the payment status.
      if ($state = VibOrderStatuses::mapToPaymentStatus($vib_order->getPaymentStatus())) {
        $this->log('Updated payment ' . $payment->id() . ' to status ' . $state . ' for amount ' . $payment->getAmount()
            ->getNumber() . '.', $order);

        $payment->setState($state);
        $payment->save();
      }

      return;
    }

    if ($state = VibOrderStatuses::mapToPaymentStatus($vib_order->getPaymentStatus())) {
      $payment->setState($state);
      if ($state == 'completed') {
        // Make sure completed time is set.
        $payment->setCompletedTime($this->time->getRequestTime());
      }
      if ($vib_order->getPaymentStatus() !== 'new') {
        $this->checkOrderTransition($order);
      }
    }

    $payment->save();
    $this->log('Added payment with status ' . $state . ' for amount ' . $payment->getAmount()
        ->getNumber() . '.', $order);

    if ($this->needsBillingProfileSync() && !$order->getBillingProfile()) {
      // Try to fetch billing profile again.
      $this->log('Syncing billing profile during notify.', $order);
      $this->syncBillingProfile($order, $vib_order);
      $needs_order_save = TRUE;
    }

    // Check if we need to update the VAT amount of the order.
    if ($this->needsVatCorrection($order)) {
      $this->addCorrectionToVat($order);
      $needs_order_save = TRUE;
    }

    if ($needs_order_save) {
      $order->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $this->log('Returned to completed step.', $order);
    $vib_order = $this->getVibOrder($order, VibOrderResponseEvent::ON_RETURN);

    if ($this->needsBillingProfileSync()) {
      $this->log('Syncing billing profile during completion.', $order);
      $this->syncBillingProfile($order, $vib_order);
    }
    parent::onReturn($order, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->log('Returned to cancel step.', $order);
    $vib_order = $this->getVibOrder($order, VibOrderResponseEvent::ON_CANCEL);

    if ($this->needsBillingProfileSync()) {
      $this->log('Syncing billing profile during cancellation.', $order);
      $this->syncBillingProfile($order, $vib_order);
    }

    $order_data = $order->getData('vib_pay');
    if (empty($order_data['error_code'])) {
      // User manually canceled the payment process.
      $this->messenger()
        ->addError($this->t('You have canceled the payment process. Please try again.'));
    }
    else {
      $error_message = VibPaymentFailureErrorCodes::getMessage($order_data['error_code']);
      $this->messenger()
        ->addError($this->t('The payment was not successful, please try again. If this problem keeps persisting contact support with following error: "@error"', [
          '@error' => $order_data['error_code'] . ': ' . $error_message,
        ]));
    }

  }

  /**
   * Checks if we need to keep a refund history.
   *
   * @return bool
   */
  public function needsRefundHistory() {
    return !empty($this->configuration['add_refunds_entities']);
  }

  /**
   * Check if order still needs transition, and transition if so.
   *
   * Orders might still be in draft state if the return URL was not called.
   * If we notice that is the case, we mark the order as completed, regardless
   * of the payment status, since that is a different state field.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   */
  protected function checkOrderTransition(OrderInterface $order) {
    // Get transition that should've been made on checkout.
    try {
      if ($completion_transition = $this->vibCommerceHelper->getOrderCompletionTransition($order)) {
        $applicable_states = array_keys($completion_transition->getFromStates());
        if (($order_state = $order->getState()) && $order_state->getId() != $completion_transition->getId() && in_array($order_state->getId(), $applicable_states)) {
          // When the payment is completed, the order should have completed the checkout process.
          // Sometimes when a user is not redirected back to the website after payment, this use case can occur.
          $order->set('cart', FALSE);
          if ($order->hasField('checkout_step')) {
            $order->set('checkout_step', 'complete');
          }
          $order->getState()->applyTransition($completion_transition);
          $order->setCompletedTime($this->time->getRequestTime());
          $order->save();
        }
      }
    }
    catch (\Exception $ex) {
      $this->logger->error($ex->getMessage(), [
        'channel' => 'Drupal order',
        'order_id' => $order ? $order->id() : NULL,
      ]);
    }
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @return bool
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function needsVatCorrection(OrderInterface $order) {
    if (empty($this->configuration['update_vat'])) {
      return FALSE;
    }

    // Check if the VAT amount differs from the on in VIB Services.
    $vib_order = $this->getVibOrder($order, VibOrderResponseEvent::ON_NOTIFY);
    $total_tax_amount = $this->getOrderTotalTaxAmount($order);

    if ($total_tax_amount->isZero()) {
      // No VAT applied on website?? Do not alter automatically. We will have to intervene manually.
      return FALSE;
    }

    return !$vib_order->getAmount()
      ->getTaxTotal()
      ->equals($total_tax_amount);
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function addCorrectionToVat(OrderInterface $order) {
    $vib_order = $this->getVibOrder($order, VibOrderResponseEvent::ON_NOTIFY);
    $order_total_tax_amount = $this->getOrderTotalTaxAmount($order);
    $vib_order_total_tax = $vib_order->getAmount()->getTaxTotal();

    // Calculate the difference in VAT.
    // Multiply the difference with negative one so the amount gets added / subtracted properly.
    $vat_difference = $order_total_tax_amount->subtract($vib_order_total_tax)
      ->multiply('-1');

    // Only auto adjust the VAT when the difference is max 5ct or -5ct.
    if (($vat_difference->isPositive() && $vat_difference->lessThanOrEqual(new Price('0.05', 'EUR'))) ||
      ($vat_difference->isNegative() && $vat_difference->greaterThanOrEqual(new Price('-0.05', 'EUR')))) {
      // Keep track of the automatic VAT change.
      $this->log('Updating the order VAT amount because it differs from the VIB Service. Commerce order VAT: ' .
        $order_total_tax_amount->getNumber() . ', VIB order VAT: ' .
        $vib_order_total_tax->getNumber() . '.', $order);

      foreach ($order->getItems() as $item) {
        /** @var \Drupal\commerce_order\Adjustment $old_adjustment */
        $adjustments = $item->getAdjustments(['tax']);
        if (!$old_adjustment = reset($adjustments)) {
          continue;
        }

        $new_adjustment = $old_adjustment->toArray();
        // Update the amount of the first VAT adjustment by subtracting the difference.
        $new_adjustment['amount'] = $old_adjustment->getAmount()
          ->add($vat_difference);
        $new_adjustment = new Adjustment($new_adjustment);
        // Add the new adjustment.
        $item->addAdjustment($new_adjustment);
        // Remove the old one.
        $item->removeAdjustment($old_adjustment);
        $item->save();
        // Update the order price.
        $order->set('total_price', $order->getTotalPrice()
          ->add($vat_difference));

//        // @todo remove, temporary logging from VIBSUP-312 / VIBSUP-342 / VIBCONF-6.
//        $this->doLog(serialize([
//          'vat_difference' => $vat_difference,
//          'old_adjustment' => $old_adjustment->toArray(),
//          'new_adjustment' => $new_adjustment->toArray(),
//        ]), $order);
        return;
      }
    }
  }

  /**
   * @return bool
   */
  protected function needsBillingProfileSync() {
    return !empty($this->configuration['update_billing_profile']);
  }

  /**
   * Builds a payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildPayment(OrderInterface $order, VibOrderResponse $vib_order) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $vib_order->getAmount()->getTotal(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $vib_order->getId(),
      'remote_state' => $vib_order->getPaymentStatus(),
    ]);

    if ($state = VibOrderStatuses::mapToPaymentStatus($vib_order->getPaymentStatus())) {
      $payment->setState($state);
    }

    return $payment;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function syncBillingProfile(OrderInterface $order, VibOrderResponse $vib_order) {
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $profile = $order->getBillingProfile() ?: $this->buildCustomerProfile($order);

    $customer = $vib_order->getCustomer();
    $billing = $vib_order->getBilling();
    $profile->address->given_name = $customer->getFirstName();
    $profile->address->family_name = $customer->getLastName();
    $profile->address->organization = $customer->getOrganisation();
    $profile->address->address_line1 = $billing->getAddress();
    $profile->address->postal_code = $billing->getPostalCode();
    $profile->address->locality = $billing->getCity();

    $country_list = $this->countryManager->getList();
    if (isset($country_list[$billing->getCountry()])) {
      $profile->address->country_code = $billing->getCountry();
    }

    // Allow other modules to alter the profile before saving.
    $event = new ProfileSyncEvent($profile, $vib_order);
    $this->eventDispatcher->dispatch($event, VibCommerceEvents::PROFILE_SYNC_PRESAVE);

    $profile->save();

    $order->setBillingProfile($profile);
  }

  /**
   * Builds a customer profile, assigned to the order's owner.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildCustomerProfile(OrderInterface $order) {
    return $this->entityTypeManager->getStorage('profile')->create([
      'uid' => $order->getCustomerId(),
      'type' => 'customer',
    ]);
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param $callback_type
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getVibOrder(OrderInterface $order, $callback_type) {
    if (!$this->vibOrder) {
      try {
        $this->log('Fetching order from VIB Services.', $order);
        $order_data = $order->getData('vib_pay');
        if (empty($order_data['vib_order_id'])) {
          throw new PaymentGatewayException('No corresponding VIB order ID found');
        }

        $this->vibOrder = $this->vibPayClient->getOrder($order_data['vib_order_id']);
        // Dispatch event.
        $event = new VibOrderResponseEvent($order, $this->vibOrder, $callback_type);
        $this->eventDispatcher->dispatch($event, VibCommerceEvents::VIB_ORDER_FETCHED);
      } catch (VibClientException $e) {
        $this->log('VIB SERVICE: Could not fetch order: ' . $e->getMessage(), $order);
        throw new PaymentGatewayException('[VIB SERVICE: Could not fetch order]: ' . $e->getMessage());
      }
    }

    return $this->vibOrder;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @return \Drupal\commerce_price\Price
   */
  protected function getOrderTotalTaxAmount(OrderInterface $order) {
    $total_tax_amount = new Price(0, 'EUR');

    foreach ($order->collectAdjustments(['tax']) as $adjustment) {
      $total_tax_amount = $total_tax_amount->add($adjustment->getAmount());
    }

    return $total_tax_amount;
  }

  /**
   * @param $message
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   */
  protected function log($message, OrderInterface $order = NULL) {
    if (empty($this->configuration['enable_logging'])) {
      return;
    }

    $this->doLog($message, $order);
  }

  /**
   * Actually push the log through.
   *
   * @param $message
   * @param \Drupal\commerce_order\Entity\OrderInterface|NULL $order
   *
   * @return void
   */
  protected function doLog($message, OrderInterface $order = NULL) {
    $this->logger->info($message, [
      'channel' => 'Drupal order',
      'order_id' => $order?->id(),
    ]);
  }

}
