<?php

namespace Drupal\vib_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides the payment process pane.
 *
 * @CommerceCheckoutPane(
 *   id = "vib_payment_process",
 *   label = @Translation("VIB Payment process"),
 *   default_step = "payment",
 *   wrapper_element = "container",
 * )
 */
class VibPaymentProcess extends PaymentProcess {

  /**
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    if ($this->order) {
      /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
      $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
      $payment_gateway = $payment_gateway_storage->load('vib_pay');
      // Force payment gateway on "vib_pay".
      $this->order->set('payment_gateway', $payment_gateway);
      $this->order->set('payment_method', NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'error_step_id' => 'login',
      'is_visible_when_amount_is_zero' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $summary[] = parent::buildConfigurationSummary();
    if (!empty($this->configuration['error_step_id'])) {
      $steps = $this->checkoutFlow->getSteps();
      $summary[] = $this->t('Step to return to when error: @step', ['@step' => $steps[$this->configuration['error_step_id']]['label']]);
      $summary[] = $this->t('Is visible when order amount equals zero: @answer',
        ['@answer' => $this->configuration['is_visible_when_amount_is_zero'] ? $this->t('Yes') : $this->t('No')]);
    }

    return implode('<br />', $summary);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options = [];
    foreach ($this->checkoutFlow->getSteps() as $step_id => $step) {
      $options[$step_id] = $step['label'];
    }

    $form['error_step_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Error step'),
      '#description' => $this->t('The step to return to when an error occurred during payment.'),
      '#options' => $options,
      '#default_value' => $this->configuration['error_step_id'],
      '#required' => TRUE,
    ];

    $form['is_visible_when_amount_is_zero'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show pane when order amount equals zero'),
      '#description' => $this->t('Check this when Commerce needs to perform a redirect to VIB Services even when the order amount equals zero.'),
      '#default_value' => $this->configuration['is_visible_when_amount_is_zero'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['error_step_id'] = $values['error_step_id'];
      $this->configuration['is_visible_when_amount_is_zero'] = $values['is_visible_when_amount_is_zero'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if ($this->order->isPaid() || (!$this->configuration['is_visible_when_amount_is_zero'] && $this->order->getTotalPrice()->isZero())) {
      // No payment is needed if the order is free or has already been paid.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getErrorStepId() {
    if (empty($this->configuration['error_step_id'])) {
      throw new \RuntimeException('Cannot get the error step ID.');
    }

    return $this->configuration['error_step_id'];
  }

}
