<?php

namespace Drupal\vib_commerce\Plugin\Commerce\TaxType;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_tax\Plugin\Commerce\TaxType\RemoteTaxTypeBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the VIB tax type.
 *
 * @CommerceTaxType(
 *   id = "vib_vat",
 *   label = "VIB remote VAT",
 * )
 */
class VibVat extends RemoteTaxTypeBase {

  protected $vibClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->vibClient = $container->get('vib_commerce.helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Prices are never displayed VAT inclusive.
    unset($form['display_inclusive']);

    // Add some info about how the VAT integration is organized.
    $form['vat_info'] = [
      '#type' => 'item',
      '#title' => $this->t('VIB VAT integration'),
      '#description' => $this->t('Each <a href="@href">product variation</a> has been provided with a field "VAT". 
The value of this field will be used to calculate the VAT during checkout. <br />The available options of this field are fetched from the VIB Services API.', [
  '@href' => Url::fromRoute('entity.commerce_product_variation_type.collection')
    ->toString(),
]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $this->configuration = [];
      $this->configuration['display_inclusive'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isDisplayInclusive() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function apply(OrderInterface $order) {
    foreach ($order->getItems() as $order_item) {
      if (!$variation = $order_item->getPurchasedEntity()) {
        // Something is really wrong?
        return;
      }
      /** @var \Drupal\vib_commerce\Client\Model\VibProduct $vib_product */
      if (!$variation->hasField('vib_vat_product') || ($variation->vib_vat_product->isEmpty())) {
        continue;
      }

      $vib_product = $variation->get('vib_vat_product')->first()->getVibObject();

      $order_item->addAdjustment(new Adjustment([
        'type' => 'tax',
        'label' => $vib_product->getName() . ' (' . $this->t('VAT') . ' ' . $vib_product->getVatPercentageLabel() . ')',
        'amount' => $order_item->getTotalPrice()
          ->multiply($vib_product->getVatPercentage()),
        'percentage' => $vib_product->getVatPercentage(),
        'source_id' => $this->parentEntity->id() . '|' . $vib_product->getCode(),
        'included' => $this->isDisplayInclusive(),
      ]));
    }
  }

}
