<?php

namespace Drupal\vib_commerce\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ViewsField("vib_commerce_remote_id_linked")
 */
class RemoteIdLinked extends FieldPluginBase {

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * Constructs a new OrderTotal instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\vib_commerce\Entity\RefundInterface $entity */
    $entity = $this->getEntity($values);

    foreach ($this->view->argument as $name => $argument) {
      // First look for an order_id argument.
      if (!$argument instanceof NumericArgument) {
        continue;
      }

      if ($argument->getField() !== 'refund_vib_refund.order_id') {
        continue;
      }
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      if ($order = $this->orderStorage->load($argument->getValue())) {
        return [
          '#type' => 'link',
          '#title' => $entity->getRemoteId(),
          '#url' => $entity->getRemoteUrl(),
          '#attributes' => [
            'target' => '_blank',
          ],
        ];
      }
    }

    return [];
  }

}
