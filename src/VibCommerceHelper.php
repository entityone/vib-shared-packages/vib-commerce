<?php

namespace Drupal\vib_commerce;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Class VibCommerceHelper.
 *
 * @package Drupal\vib_commerce
 */
class VibCommerceHelper {

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * VibCommerceHelper constructor.
   *
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->workflowManager = $workflow_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns the workflow transitions by a given workflow id.
   *
   * @param string $workflow_id
   *   The workflow id.
   *
   * @return array
   *   A list of options.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getWorkflowTransitionOptions($workflow_id) {
    $transitions = [];

    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowInterface $workflow */
    if ($workflow = $this->workflowManager->createInstance($workflow_id)) {
      foreach ($workflow->getTransitions() as $key => $transition) {
        $transitions[$key] = $transition->getLabel();
      }
    }

    return $transitions;
  }

  /**
   * Returns the order completion transition.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\state_machine\Plugin\Workflow\WorkflowTransition
   *   The completion transition.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOrderCompletionTransition(OrderInterface $order) {
    $order_type = $this->entityTypeManager->getStorage('commerce_order_type')
      ->load($order->bundle());

    /** @var \Drupal\state_machine\Plugin\Workflow\Workflow $workflow */
    $workflow = $this->workflowManager->createInstance($order_type->getWorkflowId());
    if ($transition_id = $order_type->getThirdPartySetting('vib_commerce', 'checkout_transition')) {
      return $workflow->getTransition($transition_id);
    }

    return NULL;
  }

}
