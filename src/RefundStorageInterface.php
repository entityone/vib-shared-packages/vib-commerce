<?php

namespace Drupal\vib_commerce;

/**
 * Interface RefundStorageInterface.
 *
 * @package Drupal\vib_commerce
 */
interface RefundStorageInterface {

  /**
   * Returns a refund by a given remote id.
   *
   * @param string $remote_id
   *   The remote id.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface
   *   The refund.
   */
  public function loadByRemoteId($remote_id);

}
