<?php

namespace Drupal\vib_commerce\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\Component\EventDispatcher\Event;

/**
 * Class VibOrderResponseEvent.
 *
 * @package Drupal\vib_commerce\Event
 */
class VibOrderResponseEvent extends Event {

  const ON_NOTIFY = 'on_notify';
  const ON_RETURN = 'on_return';
  const ON_CANCEL = 'on_cancel';

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The VIB order.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderResponse
   */
  protected $vibOrder;

  /**
   * The callback type.
   *
   * @var string
   */
  protected $callbackType;

  /**
   * VibOrderResponseEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   *   The VIB order.
   * @param string $callback_type
   *   The callback type.
   */
  public function __construct(OrderInterface $order, VibOrderResponse $vib_order, $callback_type) {
    $this->order = $order;
    $this->vibOrder = $vib_order;
    $this->callbackType = $callback_type;
  }

  /**
   * Returns the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Returns the VIB order.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   *   The VIB order response.
   */
  public function getVibOrder() {
    return $this->vibOrder;
  }

  /**
   * Returns the callback type.
   *
   * @return string
   *   The callback type.
   */
  public function getCallbackType() {
    return $this->callbackType;
  }

}
