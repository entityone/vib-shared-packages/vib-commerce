<?php

namespace Drupal\vib_commerce\Event;

/**
 * Class VibCommerceEvents.
 *
 * @package Drupal\vib_commerce\Event
 */
final class VibCommerceEvents {

  /**
   * Name of the event when a profile is synced.
   *
   * Dispatched just after fetching the billing information from VIB Service
   * and right before storing it in the database.
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\ProfileSyncEvent
   */
  const PROFILE_SYNC_PRESAVE = 'vib_commerce.profile_sync.presave';

  /**
   * Name of the event when the VIB order is fetched successfully from the API.
   *
   * Only dispatched in context of the payment provider.
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\VibOrderResponseEvent
   */
  const VIB_ORDER_FETCHED = 'vib_commerce.order_fetched';

  /**
   * Name of the event when a vib order is sent to the service.
   *
   * Dispatched right before sending a POST request to VIB Service
   * to create a new order.
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\VibOrderRequestEvent
   */
  const VIB_ORDER_PRE_SEND = 'vib_commerce.vib_order.pre_send';

  /**
   * Name of the event when the user returns from VIB Services.
   *
   * Dispatched before saving the order in the Return controller
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\VibOrderRequestEvent
   */
  const VIB_ORDER_PRE_RETURN = 'vib_commerce.payment.pre_return';

  /**
   * Name of the event when a vib order changes status.
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\VibOrderStatusUpdatedEvent
   */
  const VIB_ORDER_STATUS_UPDATED = 'vib_commerce.vib_order.status_updated';

  /**
   * Name of the event when a vib order payment was refunded.
   *
   * @Event
   *
   * @see \Drupal\vib_commerce\Event\VibOrderPaymentRefunded
   */
  const VIB_ORDER_PAYMENT_REFUNDED = 'vib_commerce.vib_order.payment_refunded';

}
