<?php

namespace Drupal\vib_commerce\Event;

use Drupal\profile\Entity\ProfileInterface;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\Component\EventDispatcher\Event;

/**
 * Class ProfileSyncEvent.
 *
 * @package Drupal\vib_commerce\Event
 */
class ProfileSyncEvent extends Event {

  /**
   * The profile.
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  protected $profile;

  /**
   * The VIB order.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderResponse
   */
  protected $vibOrder;

  /**
   * ProfileSyncEvent constructor.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   *   The VIB order.
   */
  public function __construct(ProfileInterface $profile, VibOrderResponse $vib_order) {
    $this->profile = $profile;
    $this->vibOrder = $vib_order;
  }

  /**
   * Returns the profile.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The profile entity.
   */
  public function getProfile() {
    return $this->profile;
  }

  /**
   * Returns the VIB order.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   *   The VIB order response.
   */
  public function getVibOrder() {
    return $this->vibOrder;
  }

}
