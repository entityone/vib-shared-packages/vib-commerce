<?php

namespace Drupal\vib_commerce\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\vib_commerce\Client\Model\VibOrderRequest;
use Drupal\Component\EventDispatcher\Event;

/**
 * Class VibOrderRequestEvent.
 *
 * @package Drupal\vib_commerce\Event
 */
class VibOrderRequestEvent extends Event {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The VIB order.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderRequest
   */
  protected $vibOrder;

  /**
   * VibOrderRequestEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderRequest $vib_order
   *   The VIB order.
   */
  public function __construct(OrderInterface $order, VibOrderRequest $vib_order) {
    $this->order = $order;
    $this->vibOrder = $vib_order;
  }

  /**
   * Returns the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Returns the VIB order.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderRequest
   *   The VIB order request.
   */
  public function getVibOrder() {
    return $this->vibOrder;
  }

}
