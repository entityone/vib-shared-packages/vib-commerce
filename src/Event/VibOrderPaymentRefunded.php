<?php

namespace Drupal\vib_commerce\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\Component\EventDispatcher\Event;

/**
 * Class VibOrderPaymentRefunded.
 *
 * @package Drupal\vib_commerce\Event
 */
class VibOrderPaymentRefunded extends Event {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The VIB order.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderResponse
   */
  protected $vibOrder;

  /**
   * VibOrderPaymentRefunded constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   *   The VIB order.
   */
  public function __construct(OrderInterface $order, PaymentInterface $payment, VibOrderResponse $vib_order) {
    $this->order = $order;
    $this->payment = $payment;
    $this->vibOrder = $vib_order;
  }

  /**
   * Returns the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Returns the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment entity.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Returns the VIB order.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   *   The VIB order response.
   */
  public function getVibOrder() {
    return $this->vibOrder;
  }

}
