<?php

namespace Drupal\vib_commerce\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\vib_commerce\Client\Model\VibOrderResponse;
use Drupal\Component\EventDispatcher\Event;

/**
 * Class VibOrderStatusUpdatedEvent.
 *
 * @package Drupal\vib_commerce\Event
 */
class VibOrderStatusUpdatedEvent extends Event {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The VIB order.
   *
   * @var \Drupal\vib_commerce\Client\Model\VibOrderResponse
   */
  protected $vibOrder;

  /**
   * List of all refunds.
   *
   * @var \Drupal\vib_commerce\Entity\RefundInterface[]
   */
  protected $refunds;

  /**
   * VibOrderRequestEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\vib_commerce\Client\Model\VibOrderResponse $vib_order
   *   The VIB order.
   * @param \Drupal\vib_commerce\Entity\RefundInterface[] $refunds
   *   List of all refunds.
   */
  public function __construct(OrderInterface $order, VibOrderResponse $vib_order, array $refunds) {
    $this->order = $order;
    $this->vibOrder = $vib_order;
    $this->refunds = $refunds;
  }

  /**
   * Returns the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Returns the VIB order.
   *
   * @return \Drupal\vib_commerce\Client\Model\VibOrderResponse
   *   The VIB order response.
   */
  public function getVibOrder() {
    return $this->vibOrder;
  }

  /**
   * Returns all refunds.
   *
   * @return \Drupal\vib_commerce\Entity\RefundInterface[]
   *   A list of refunds.
   */
  public function getRefunds() {
    return $this->refunds;
  }

}
