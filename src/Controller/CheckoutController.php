<?php

namespace Drupal\vib_commerce\Controller;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Controller\PaymentCheckoutController;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\vib_commerce\Client\VibPayClientInterface;
use Drupal\vib_commerce\Entity\Refund;
use Drupal\vib_commerce\Event\VibCommerceEvents;
use Drupal\vib_commerce\Event\VibOrderPreReturnEvent;
use Drupal\vib_commerce\Event\VibOrderStatusUpdatedEvent;
use Drupal\vib_service\Client\VibClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class CheckoutController.
 *
 * @package Drupal\vib_commerce\Controller
 */
class CheckoutController extends PaymentCheckoutController {

  protected $request;
  protected $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->request = $container->get('request_stack')?->getCurrentRequest();
    $instance->client = $container->get('vib_commerce.pay_client');

    return $instance;
  }

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account) {
    if (!$this->request->query->get('OrderId') || !$this->request->query->get('ShopOrderId')) {
      return AccessResult::forbidden();
    }

    // Try to load the order.
    $order_id = $this->request->query->get('ShopOrderId');
    if (!$order = Order::load($order_id)) {
      return AccessResult::forbidden();
    }

    // Check if the order has the given vib order reference.
    $order_data = $order->getData('vib_pay');
    if (empty($order_data['vib_order_id'])) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    $vib_order_id = $this->request->query->get('OrderId');
    if ($order_data['vib_order_id'] != $vib_order_id) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    // Do not allow when we are not awaiting redirect.
    if (!$order_data['awaiting_redirect']) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    if (!$account->isAuthenticated()) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    // The user can only check out their own non-empty orders.
    if ($account->id() !== $order->getCustomerId()) {
      return AccessResult::forbidden()->addCacheableDependency($order);
    }

    return AccessResult::allowed()->addCacheableDependency($order);
  }

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function checkAccessOrderNotification(RouteMatchInterface $route_match, AccountInterface $account) {
    $body = Json::decode($this->request->getContent());
    if (empty($body['ShopOrderId']) || empty($body['OrderId'])) {
      return AccessResult::forbidden('Empty shop order id and/or order id');
    }

    // Try to load the order.
    $order_id = $body['ShopOrderId'];
    if (!$order = Order::load($order_id)) {
      return AccessResult::forbidden('Invalid order');
    }

    // Check if the order has the given vib order reference.
    $order_data = $order->getData('vib_pay');
    if (empty($order_data['vib_order_id'])) {
      return AccessResult::forbidden();
    }

    $vib_order_id = $body['OrderId'];
    if ($order_data['vib_order_id'] != $vib_order_id) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onReturn() {
    $order_id = $this->request->query->get('ShopOrderId');
    $order = Order::load($order_id);

    // Mark order as not "awaiting redirect".
    $order_data = $order->getData('vib_pay');
    $order_data['awaiting_redirect'] = FALSE;
    $order->setData('vib_pay', $order_data);
    // Send an event to alter the order before saving if needed.
    $this->eventDispatcher->dispatch(new VibOrderPreReturnEvent($order), VibCommerceEvents::VIB_ORDER_PRE_RETURN);
    $order->save();

    // Redirect and let commerce handle the further process.
    $url = Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ])->toString();

    return new RedirectResponse($url);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onCancel() {
    $order_id = $this->request->query->get('ShopOrderId');
    $order = Order::load($order_id);

    // Mark order as not "awaiting redirect".
    $order_data = $order->getData('vib_pay');
    $order_data['awaiting_redirect'] = FALSE;
    $order_data['error_code'] = $this->request->query->get('Error');
    $order->setData('vib_pay', $order_data);
    $order->save();

    // Redirect and let commerce handle the further process.
    $url = Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ])->toString();

    return new RedirectResponse($url);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function onOrderNotify() {
    // No caching!
    \Drupal::service('page_cache_kill_switch')->trigger();

    $body = Json::decode($this->request->getContent());
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = Order::load($body['ShopOrderId']);
    $order_data = $order->getData('vib_pay');

    try {
      /** @var \Drupal\vib_commerce\Plugin\Commerce\PaymentGateway\VibPay $payment_gateway */
      $payment_gateway = PaymentGateway::load('vib_pay')->getPlugin();
      $vib_order = $this->client->getOrder($order_data['vib_order_id']);

      $all_refunds = [];
      $new_refunds = [];

      if ($payment_gateway->needsRefundHistory() && ($vib_order->isCanceled() || $vib_order->isPartiallyCanceled())) {
        // We need to sync refund entities.
        if ($refunds = $this->client->getOrderRefunds($vib_order->getId())) {
          // Empty referenced refunds so we can populate them again.
          $order->get('refund')->setValue([]);
          /** @var \Drupal\vib_commerce\RefundStorageInterface $storage */
          $storage = $this->entityTypeManager->getStorage('vib_refund');

          foreach ($refunds as $refund) {
            // Check if we need to create a new refund entity.
            if (!$entity = $storage->loadByRemoteId($refund->getId())) {
              /** @var \Drupal\vib_commerce\Entity\RefundInterface $entity */
              $entity = Refund::create();
              // Only set the remote ID when this is a new entity.
              // Once created, this cannot change anymore.
              $entity->setRemoteId($refund->getId());
              $entity->setOrder($order);
            }

            // Sync values with API values.
            Refund::syncWithApi($refund, $entity);

            $entity->save();
            // Attach the API object to be used in the event subscribers.
            $entity->setRefundApiObject($refund);
            $all_refunds[] = $entity;

            // Attach refund to order.
            $order->get('refund')->appendItem($entity);
          }
          $order->save();
        }
      }

      // Dispatch event.
      $event = new VibOrderStatusUpdatedEvent($order, $vib_order, $all_refunds);
      $this->eventDispatcher->dispatch($event, VibCommerceEvents::VIB_ORDER_STATUS_UPDATED);

      return new Response('Order transition applied');
    } catch (VibClientException $e) {
      throw new BadRequestHttpException('Order ' . $order_data['vib_order_id'] . ' could not be fetched');
    }
  }

}
